#ifndef GHETTO_BYTE_HPP
#define GHETTO_BYTE_HPP
#include <cstring>
#include <iomanip>
#include <sstream>
#include <string>

namespace ghetto {
template <typename CharT, typename DataT>
std::basic_string<CharT> convertDataToBytes(DataT const* data,
                                            std::size_t element_count = 1) {
  return std::basic_string<CharT>(reinterpret_cast<CharT const*>(data),
                                  sizeof(DataT) * element_count);
}

template <typename CharT, typename DataT>
std::vector<DataT> convertBytesToData(std::basic_string<CharT> const& bytes) {
  std::size_t element_count = bytes.length() / sizeof(DataT);
  if (bytes.length() % sizeof(DataT) != 0)
    throw "Byte size not a multiple of element size";
  std::vector<DataT> data(element_count);
  memcpy(const_cast<void*>(reinterpret_cast<void const*>(data.data())),
         reinterpret_cast<void const*>(bytes.data()), bytes.length());
  return data;
}

template <typename CharT, typename DataT>
void convertBytesToData(std::vector<DataT>& data,
                        std::basic_string<CharT> const& bytes) {
  std::size_t element_count = bytes.length() / sizeof(DataT);
  if (data.size() != element_count)
    throw "Destination vector length and source byte size mismatch";
  memcpy(const_cast<void*>(reinterpret_cast<void const*>(data.data())),
         reinterpret_cast<void const*>(bytes.data()), bytes.length());
}

template <typename CharT>
std::string getHexadecimalRepresentation(
    std::basic_string<CharT> const& bytes) {
  std::stringstream stream;
  stream << std::hex;
  for (std::size_t i = 0; i < bytes.length(); ++i) {
    stream << std::setw(2) << std::setfill('0')
           << static_cast<int>(static_cast<unsigned char>(bytes[i]));
  }
  return stream.str();
}
}  // namespace ghetto
#endif
