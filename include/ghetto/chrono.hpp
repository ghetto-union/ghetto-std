#ifndef GHETTO_CHRONO_HPP
#define GHETTO_CHRONO_HPP
#include <chrono>

namespace ghetto {
template <class ToDuration, class TimePoint>
constexpr std::size_t getDuration(TimePoint const& t0, TimePoint const& t1) {
  return (std::chrono::duration_cast<ToDuration>(
              static_cast<std::chrono::duration<double>>(t1 - t0)))
      .count();
}

template <class ClockT, class ToDuration, class TimePoint>
std::size_t updateTimePoint(TimePoint& t) {
  auto current_time = ClockT::now();
  std::size_t duration = getDuration<ToDuration>(t, current_time);
  t = current_time;
  return duration;
}

template <class ToDuration, class TimePoint>
constexpr std::size_t getTimeSinceEpoch(TimePoint const& t) {
  return (std::chrono::duration_cast<ToDuration>(t.time_since_epoch())).count();
}

template <class ToDuration, class ClockT>
constexpr std::size_t getTimeSinceEpoch() {
  return getTimeSinceEpoch<ToDuration>(ClockT::now());
}
}  // namespace ghetto
#endif
