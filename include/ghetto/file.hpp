#ifndef GHETTO_FILE_HPP
#define GHETTO_FILE_HPP
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace ghetto {
std::string getPathRelativeToExecutable(std::string const&);

template <typename charT>
std::basic_string<charT> readFileStream(std::string const& path) {
  std::basic_ifstream<charT> stream;
  stream.exceptions(std::basic_ifstream<charT>::failbit |
                    std::basic_ifstream<charT>::badbit);
  try {
    stream.open(getPathRelativeToExecutable(path), std::basic_ios<charT>::in);
    std::basic_stringstream<charT> content_stream;
    content_stream << stream.rdbuf();
    stream.close();
    return content_stream.str();
  } catch (typename std::basic_ifstream<charT>::failure e) {
    std::cerr << "Failed to read file [" << path << "]." << std::endl;
    std::cerr << "Reason: " << e.what() << std::endl;
    std::rethrow_exception(std::current_exception());
  }
}

template <typename charT>
std::basic_string<charT> readFile(std::string const& path) {
  std::basic_ifstream<charT> stream;
  stream.exceptions(std::basic_ifstream<charT>::failbit |
                    std::basic_ifstream<charT>::badbit);
  try {
    // use binary mode to read \r\n as is during stream.read(), otherwise the
    // input position indicator will run over EOF
    stream.open(getPathRelativeToExecutable(path),
                std::basic_ios<charT>::in | std::ios_base::binary);
    std::basic_string<charT> contents;
    stream.seekg(0, std::basic_ios<charT>::end);
    contents.resize(stream.tellg());
    stream.seekg(0, std::basic_ios<charT>::beg);
    stream.read(&contents[0], contents.size());
    stream.close();
    return contents;
  } catch (typename std::basic_ifstream<charT>::failure e) {
    std::cerr << "Failed to read file [" << path << "]." << std::endl;
    std::cerr << "Reason: " << e.what() << std::endl;
    std::rethrow_exception(std::current_exception());
  }
}
}  // namespace ghetto
#endif
