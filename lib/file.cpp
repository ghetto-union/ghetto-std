#include "ghetto/file.hpp"
#include <boost/dll/runtime_symbol_info.hpp>
#include <exception>
#include <fstream>
#include <iostream>

namespace ghetto {
std::string getPathRelativeToExecutable(std::string const& relative_path) {
  if (boost::filesystem::path(relative_path).is_complete())
    return relative_path;
  return boost::dll::program_location()
      .parent_path()
      .append(relative_path)
      .string();
}
}  // namespace ghetto
